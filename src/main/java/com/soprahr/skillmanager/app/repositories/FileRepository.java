package com.soprahr.skillmanager.app.repositories;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soprahr.skillmanager.app.entities.FileEntity;
public interface FileRepository extends JpaRepository<FileEntity, String>{
	Optional<FileEntity> findByUserId(String userId);
}
