package com.soprahr.skillmanager.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soprahr.skillmanager.app.entities.SiteEntity;

public interface SiteRepository extends  JpaRepository<SiteEntity, Long>{
}
