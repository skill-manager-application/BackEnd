package com.soprahr.skillmanager.app.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.soprahr.skillmanager.app.entities.RoleEntity;

@Repository
public interface RoleRepository extends  JpaRepository<RoleEntity, Long>{
	List<RoleEntity> findByRoleName(String roleName);
}
