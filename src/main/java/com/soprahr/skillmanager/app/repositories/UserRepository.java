package com.soprahr.skillmanager.app.repositories;



import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.soprahr.skillmanager.app.entities.UserEntity;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
	UserEntity findByEmail(String email);
	UserEntity findByUserId(String Userid);
	
	//sql native
	@Query(value="select * from user",nativeQuery = true)
	Page<UserEntity> findAllUser(Pageable pageableRequest);
	List<UserEntity> findUsersByRoleIdRole(Long roleId);
	Page<UserEntity> findUsersByDepartmentId(Pageable pageableRequest,Long DepartmentId);
	List<UserEntity> findUsersByDepartmentId(Long DepartmentId);

	@Query(value="SELECT * FROM user u WHERE u.email LIKE %:search%",nativeQuery = true)
	Page<UserEntity> findAllUserCriteria(Pageable pageableRequest,@Param("search") String search);
	
} 
