package com.soprahr.skillmanager.app.responses;

import com.soprahr.skillmanager.app.entities.RoleEntity;

public class UserResponses {
	private String userId;
	private String firstname;
	private String lastname;
	private String email;
	private RoleEntity role;
	private long departmentId;
	private String departmentDomain;
	private long siteId;
	private String siteName;


	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public RoleEntity getRole() {
		return role;
	}
	public void setRole(RoleEntity role) {
		this.role = role;
	}
	public long getSiteId() {
		return siteId;
	}
	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}
	public long getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentDomain() {
		return departmentDomain;
	}
	public void setDepartmentDomain(String departmentDomain) {
		this.departmentDomain = departmentDomain;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

}