package com.soprahr.skillmanager.app.responses;

import java.util.List;


public class UserListResponse {
    private List<UserResponses> userResponses;
    private int totalPages;
	public List<UserResponses> getUserDtoList() {
		return userResponses;
	}
	public void setUserDtoList(List<UserResponses> userResponses) {
		this.userResponses = userResponses;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
