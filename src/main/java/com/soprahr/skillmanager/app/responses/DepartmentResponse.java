package com.soprahr.skillmanager.app.responses;

import com.soprahr.skillmanager.app.entities.Type;

public class DepartmentResponse {
	private long id;
	private String domain;
	private Long siteId;
	private Type departmentType;
	private long departmentMotherId;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public Type getDepartmentType() {
		return departmentType;
	}
	public void setDepartmentType(Type departmentType) {
		this.departmentType = departmentType;
	}
	
	public long getDepartmentMotherId() {
		return departmentMotherId;
	}
	public void setDepartmentMotherId(long departmentMotherId) {
		this.departmentMotherId = departmentMotherId;
	}
	public Long getSiteId() {
		return siteId;
	}
	public void setSiteId(Long siteId) {
		this.siteId = siteId;
	}

}
