package com.soprahr.skillmanager.app.services;

import org.springframework.web.multipart.MultipartFile;

import com.soprahr.skillmanager.app.entities.FileEntity;

public interface FileService {
	  FileEntity storeFile(MultipartFile file,String userId);
	  FileEntity getFile(String fileId);
}
