package com.soprahr.skillmanager.app.services.impl;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.soprahr.skillmanager.app.entities.DepartmentEntity;
import com.soprahr.skillmanager.app.entities.SiteEntity;
import com.soprahr.skillmanager.app.entities.UserEntity;
import com.soprahr.skillmanager.app.repositories.DepartmentRepository;
import com.soprahr.skillmanager.app.repositories.SiteRepository;
import com.soprahr.skillmanager.app.repositories.UserRepository;
import com.soprahr.skillmanager.app.services.DepartmentService;
import com.soprahr.skillmanager.app.shared.Utils;
import com.soprahr.skillmanager.app.shared.dto.DepartmentDto;
import com.soprahr.skillmanager.app.shared.dto.UserDto;
import com.soprahr.skillmanager.app.entities.Type;


@Service
public class DepartmentServiceImpl implements DepartmentService{
	
	Type type = Type.Direction;
	
	@Autowired
	Utils utils;
	
	@Autowired
	UserRepository userRepositroy;
	
	@Autowired
	DepartmentRepository departmentRepository;
	
	@Autowired
	SiteRepository siteRepository;
	
	@Override
	public List<DepartmentDto> getAllDepartments(String email) {
		UserEntity checkUser = userRepositroy.findByEmail(email);
	    List<DepartmentDto> DepartmentsListDto = new ArrayList<>();
	    List<DepartmentEntity> departments = (List<DepartmentEntity>) departmentRepository.findAll();
		 for (DepartmentEntity departmentEntity : departments) {
			 
			 if(departmentEntity.getDepartmentType().equals(type) &&	 
					    !(checkUser.getRole().getRoleName().equals("Manager") ||
					    	      checkUser.getRole().getRoleName().equals("Admin"))){
					continue;
				 }else {
					DepartmentDto dept = new DepartmentDto();
					BeanUtils.copyProperties(departmentEntity, dept);
					dept.setSiteId(departmentEntity.getSite().getId());
			        DepartmentsListDto.add(dept);
		    }
		}
		 return DepartmentsListDto;
		
	}

	@Override
	public DepartmentDto createDepartment(DepartmentDto departmentDto, String email) {
		
		UserEntity currentUser = userRepositroy.findByEmail(email);
		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = modelMapper.map(currentUser, UserDto.class);
		
		if (userDto.getRole().equals("Collaboratrur"))
			throw new RuntimeException("User Role Error !!");
		
		for (DepartmentEntity department : departmentRepository.findAll()) {
			if(department.getDomain().equals(departmentDto.getDomain()) && department.getSite().getId() ==departmentDto.getSiteId() ) {
				throw new RuntimeException("Department Already Exist");
			}
		}
		if (departmentDto.getSiteId() == 0)
			throw new RuntimeException("site not found error!!");
		
		SiteEntity site = siteRepository.findById(departmentDto.getSiteId()).get();
		departmentDto.setId(null);
		DepartmentEntity departmentEntity = modelMapper.map(departmentDto, DepartmentEntity.class);
		departmentEntity.setSite(site);
		DepartmentEntity newDepartment = departmentRepository.save(departmentEntity);
		DepartmentDto department = modelMapper.map(newDepartment, DepartmentDto.class);
		return department;
		}

	@Override
	public DepartmentDto getDepartment(long id,String email) {
		UserEntity checkUser = userRepositroy.findByEmail(email);
		
		DepartmentEntity departmentEntity = departmentRepository.findById(id).get();
		if(departmentEntity.getDepartmentType().equals(type) &&
			    !(checkUser.getRole().getRoleName().equals("Manager") ||
			    	      checkUser.getRole().getRoleName().equals("Admin")))
			return null;
		ModelMapper modelMapper = new ModelMapper();
		DepartmentDto department = modelMapper.map(departmentEntity, DepartmentDto.class);
		return department;
	}

	@Override
	public void DeleteDepartment(long id) {
		DepartmentEntity departmentEntity = departmentRepository.findById(id).get();
		if (departmentEntity == null)
			throw new UsernameNotFoundException("ID : " +id);
		departmentRepository.delete(departmentEntity);
	}

	@Override
	public DepartmentDto updateDepartment(long id, DepartmentDto departmentDto) {
		DepartmentEntity departmentEntity = departmentRepository.findById(id).get();
		if (departmentEntity == null) throw new RuntimeException("Department Not Found");
		departmentEntity.setDomain(departmentDto.getDomain());
		departmentEntity.setDepartmentType(departmentDto.getDepartmentType());
		departmentEntity.setDepartmentMotherId(departmentDto.getDepartmentMotherId());
		DepartmentEntity newDepartment = departmentRepository.save(departmentEntity);
		ModelMapper modelMapper = new ModelMapper();
		DepartmentDto department = modelMapper.map(newDepartment, DepartmentDto.class);
		return department;
	}
	
	@Override
	public List<DepartmentDto> getDepartmentsBySiteId(long siteId,String email) {
		UserEntity checkUser = userRepositroy.findByEmail(email);	
		List<DepartmentDto> listDepartemntDto = new ArrayList<>();
		List<DepartmentEntity> listDepartment = departmentRepository.findDepartmentBySiteId(siteId);
		ModelMapper modelMapper = new ModelMapper();
		for (DepartmentEntity departmentEntity : listDepartment) {
			if(departmentEntity.getDepartmentType().equals(type) &&
					 
				    !(checkUser.getRole().getRoleName().equals("Manager") ||
				    	      checkUser.getRole().getRoleName().equals("Admin"))){
				continue;
			 }else {
			DepartmentDto department = modelMapper.map(departmentEntity, DepartmentDto.class);
			listDepartemntDto.add(department);
		}
		}
		return listDepartemntDto ;
	}

}
