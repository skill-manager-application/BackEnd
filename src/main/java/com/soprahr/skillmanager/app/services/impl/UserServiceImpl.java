package com.soprahr.skillmanager.app.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.soprahr.skillmanager.app.entities.DepartmentEntity;
import com.soprahr.skillmanager.app.entities.UserEntity;
import com.soprahr.skillmanager.app.repositories.DepartmentRepository;
import com.soprahr.skillmanager.app.repositories.RoleRepository;
import com.soprahr.skillmanager.app.repositories.UserRepository;
import com.soprahr.skillmanager.app.services.UserService;
import com.soprahr.skillmanager.app.shared.Utils;
import com.soprahr.skillmanager.app.shared.dto.UserDto;
import com.soprahr.skillmanager.app.shared.dto.UserDtoListResponse;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	DepartmentRepository departmentRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	Utils utils;
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public UserDto createUser(UserDto user) {
		UserEntity checkUser = userRepository.findByEmail(user.getEmail());
		if (checkUser != null)
			throw new RuntimeException("User Already Exists !");
		
		if (user.getRole() == null ||roleRepository.findById(user.getRole().getIdRole()) == null ) {
			throw new RuntimeException("Role Not Found !!");
		}
		ModelMapper modelMapper = new ModelMapper();
		UserEntity userEntity = modelMapper.map(user, UserEntity.class);
		userEntity.setEncryptedPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userEntity.setUserId(utils.GenerateStringId(32));
		List<DepartmentEntity> dep = departmentRepository.findDepartmentBySiteId(user.getSite().getId());
		dep.forEach((el)->{
			if(el.getDomain().equals("Assining")) 
			{
				userEntity.setDepartment(el);
			}
		});
		UserEntity newUser = userRepository.save(userEntity);
		UserDto userDto = modelMapper.map(newUser, UserDto.class);
		return userDto;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		UserEntity userEntity = userRepository.findByEmail(email);
		if (userEntity == null)
			throw new UsernameNotFoundException(email);
		return new User(userEntity.getEmail(), userEntity.getEncryptedPassword(), new ArrayList<>());
	}
	

	@Override
	public UserDto getUser(String email) {
		UserEntity userEntity = userRepository.findByEmail(email);
		if (userEntity == null)
			throw new UsernameNotFoundException(email);
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userEntity, userDto);
		return userDto;
	}

	@Override
	public UserDto getUserByUserId(String UserId) {
		UserEntity userEntity = userRepository.findByUserId(UserId);
		if (userEntity == null)
			throw new UsernameNotFoundException(UserId);
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userEntity, userDto);
		return userDto;
	}

	@Override
	public UserDto updateUser(String id, UserDto userDto) {
		UserEntity userEntity = userRepository.findByUserId(id);
		if (userEntity == null)
			throw new UsernameNotFoundException(id);
		userEntity.setFirstname(userDto.getFirstname());
		userEntity.setLastname(userDto.getLastname());
		userEntity.setRole(userDto.getRole());
		userEntity.setSite(userDto.getSite());
		userEntity.setDepartment(userDto.getDepartment());
		UserEntity UserUpdated = userRepository.save(userEntity);
		UserDto user = new UserDto();
		BeanUtils.copyProperties(UserUpdated, user);
		return user;
	}

	@Override
	public void DeleteUser(String UserId) {
		UserEntity userEntity = userRepository.findByUserId(UserId);
		if (userEntity == null)
			throw new UsernameNotFoundException(UserId);
		userRepository.delete(userEntity);
	}

	@Override
	public UserDtoListResponse getUsers(int page, int limit, String search) {
	    if (page > 0)
	        page -= 1;
	    List<UserDto> userDtoList = new ArrayList<>();
	    Pageable pageableRequest = PageRequest.of(page, limit);
	    Page<UserEntity> userPage;
	    if (search.isEmpty()) {
	        userPage = userRepository.findAllUser(pageableRequest);
	    } else {
	        userPage = userRepository.findAllUserCriteria(pageableRequest, search);
	    }
	    List<UserEntity> users = userPage.getContent();
	    for (UserEntity userEntity : users) {
	        ModelMapper modelMapper = new ModelMapper();
	        UserDto user = modelMapper.map(userEntity, UserDto.class);
	        userDtoList.add(user);
	    }
	    UserDtoListResponse response = new UserDtoListResponse();
	    response.setUserDtoList(userDtoList);
	    response.setTotalPages(userPage.getTotalPages());
	    return response;
	}
	
	
	
	@Override
	public List<UserDto> getUsersNoLimit(String email) {
	    List<UserDto> userDtoList = new ArrayList<>();
		UserEntity checkUser = userRepository.findByEmail(email);
		List<UserEntity> users = (List<UserEntity>) userRepository.findAll();
		if(checkUser.getRole().getRoleName().equals("Admin")) {
			for (UserEntity userEntity : users) {
		        ModelMapper modelMapper = new ModelMapper();
		        UserDto user = modelMapper.map(userEntity, UserDto.class);
		        userDtoList.add(user);
		    }
		}else {
			for (UserEntity userEntity : users) {
		       if(!userEntity.getRole().getRoleName().equals("Admin") && (userEntity.getSite().getId() == checkUser.getSite().getId())) {
		    	   ModelMapper modelMapper = new ModelMapper();
			        UserDto user = modelMapper.map(userEntity, UserDto.class);
			        userDtoList.add(user);
		       }
		    }
		}
		return userDtoList;

	}
	
	@Override
	public UserDtoListResponse getUsersByDeprtmentId(long departmentId,int page,int limit,String email) {
		UserEntity userRequestor = userRepository.findByEmail(email);
		if (userRequestor == null)
			throw new UsernameNotFoundException(email);
		if (page > 0)
	        page -= 1;
	    List<UserDto> userDtoList = new ArrayList<>();
	    Pageable pageableRequest = PageRequest.of(page, limit);
	    Page<UserEntity> userPage;
        userPage = userRepository.findUsersByDepartmentId(pageableRequest,departmentId);
        List<UserEntity> users = userPage.getContent();
    	if(userRequestor.getRole().getRoleName().equals("Admin")) {
    	    for (UserEntity userEntity : users) {
    	        ModelMapper modelMapper = new ModelMapper();
    	        UserDto user = modelMapper.map(userEntity, UserDto.class);
    	        userDtoList.add(user);
    	    }
    	}else {
    		 for (UserEntity userEntity : users) {
     	        if(!userEntity.getRole().getRoleName().equals("Admin") && !userEntity.getRole().getRoleName().equals("RH")) {
     	        	ModelMapper modelMapper = new ModelMapper();
         	        UserDto user = modelMapper.map(userEntity, UserDto.class);
         	        userDtoList.add(user);
     	        }
     	    }
    	}

	    UserDtoListResponse response = new UserDtoListResponse();
	    response.setUserDtoList(userDtoList);
	    response.setTotalPages(userPage.getTotalPages());
	    return response;
	}
	
	@Override
	public List<UserDto> getUsersByDeprtmentIdUserResponse(long departmentId) {
	    List<UserDto> userDtoList = new ArrayList<>();
	    List<UserEntity> users = userRepository.findUsersByDepartmentId(departmentId);
	    for (UserEntity userEntity : users) {
	        ModelMapper modelMapper = new ModelMapper();
	        UserDto user = modelMapper.map(userEntity, UserDto.class);
	        userDtoList.add(user);
	    }
	    return userDtoList;
	}

}
