package com.soprahr.skillmanager.app.services.impl;

import java.lang.reflect.Type;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.soprahr.skillmanager.app.entities.RoleEntity;
import com.soprahr.skillmanager.app.entities.UserEntity;
import com.soprahr.skillmanager.app.repositories.RoleRepository;
import com.soprahr.skillmanager.app.repositories.UserRepository;
import com.soprahr.skillmanager.app.services.RoleService;
import com.soprahr.skillmanager.app.shared.Utils;
import com.soprahr.skillmanager.app.shared.dto.RoleDto;
import com.soprahr.skillmanager.app.shared.dto.UserDto;

@Service
public class RoleServiceImpl implements RoleService{
	
	@Autowired
	RoleRepository roleRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	Utils utils;

	@Override
	public List<RoleDto> getAllRoles(String email) {
		UserEntity currentUser = userRepository.findByEmail(email);
		
		List<RoleEntity> roles =  roleRepository.findAll();
		Type listType = new TypeToken<List<RoleDto>>() {}.getType();
		List<RoleDto> roleDto = new ModelMapper().map(roles, listType);
		return roleDto;
	}

	@Override
	public RoleDto createRole(RoleDto roleDto, String email) {

		UserEntity currentUser = userRepository.findByEmail(email);
		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = modelMapper.map(currentUser, UserDto.class);
		
		
		for (RoleEntity role : roleRepository.findAll()) {
			if(role.getRoleName().equals(roleDto.getRoleName()) && role.getJob_title().equals(roleDto.getJob_title())) {
				throw new RuntimeException("Role Already Exist");
			}
		}
		RoleEntity roleEntity = modelMapper.map(roleDto, RoleEntity.class);
		RoleEntity newRole = roleRepository.save(roleEntity);
		RoleDto role = modelMapper.map(newRole, RoleDto.class);
		
		return role;
	}

	@Override
	public RoleDto getRole(long id) {
		RoleEntity roleEntity = roleRepository.findById(id).get();
		ModelMapper modelMapper = new ModelMapper();
		RoleDto role = modelMapper.map(roleEntity, RoleDto.class);
		return role;
	}

	@Override
	public void DeleteRole(long roleId) {
		RoleEntity roleEntity = roleRepository.findById(roleId).get();
		if (roleEntity == null) throw new RuntimeException("Address Not Found");
		roleRepository.delete(roleEntity);		
	}
	
	@Override
	public RoleDto updateRole(long roleId,RoleDto roleDto) {
		RoleEntity roleEntity = roleRepository.findById(roleId).get();
		if (roleEntity == null) throw new RuntimeException("Role Not Found");
		roleEntity.setRoleName(roleDto.getRoleName());
		roleEntity.setJob_title(roleDto.getJob_title());
		RoleEntity updatedRole = roleRepository.save(roleEntity);
		ModelMapper modelMapper = new ModelMapper();
		RoleDto role = modelMapper.map(updatedRole, RoleDto.class);
		return role;
	}

	@Override
	public void DeleteAll(String roleName) {
		List<RoleEntity> roles = roleRepository.findByRoleName(roleName);
		roleRepository.deleteAll(roles);
	}


	
	

}
