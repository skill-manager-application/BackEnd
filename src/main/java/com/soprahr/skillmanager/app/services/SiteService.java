package com.soprahr.skillmanager.app.services;

import java.util.List;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;

import com.soprahr.skillmanager.app.shared.dto.SiteDto;

public interface SiteService {
	List<SiteDto> getAllSites(String email);
	SiteDto createSite(SiteDto siteDto,String email);
	SiteDto getSite(long id);
	void deleteSite(long id) throws NotFoundException;
	SiteDto updateSite(long id ,SiteDto siteDto) throws NotFoundException;
}
