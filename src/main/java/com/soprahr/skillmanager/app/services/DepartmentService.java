package com.soprahr.skillmanager.app.services;

import java.util.List;

import javax.validation.Valid;

import com.soprahr.skillmanager.app.requests.DepartmentRequest;
import com.soprahr.skillmanager.app.shared.dto.DepartmentDto;

public interface DepartmentService {
	List<DepartmentDto>getAllDepartments(String email);
	DepartmentDto createDepartment(@Valid DepartmentDto departmentRequest,String email);
	DepartmentDto getDepartment(long id,String email);
	void DeleteDepartment(long id);
	DepartmentDto updateDepartment(long id ,DepartmentDto departmentDto);
	List<DepartmentDto> getDepartmentsBySiteId(long siteId,String email);
}
