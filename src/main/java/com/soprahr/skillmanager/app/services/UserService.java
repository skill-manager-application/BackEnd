package com.soprahr.skillmanager.app.services;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.soprahr.skillmanager.app.shared.dto.UserDto;
import com.soprahr.skillmanager.app.shared.dto.UserDtoListResponse;

public interface UserService extends UserDetailsService {
	
	UserDto createUser(UserDto userDto);
	UserDto getUser(String email);
	UserDto getUserByUserId(String UserId);
	UserDto updateUser(String id,UserDto userDto);
	void DeleteUser(String UserId);
	UserDtoListResponse getUsers(int page,int limit,String search);
	UserDtoListResponse getUsersByDeprtmentId(long departmentId,int page,int limit,String email);
	List<UserDto> getUsersByDeprtmentIdUserResponse(long departmentId);
	List<UserDto> getUsersNoLimit(String email);
}
