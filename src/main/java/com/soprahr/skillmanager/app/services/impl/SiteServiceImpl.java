package com.soprahr.skillmanager.app.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.soprahr.skillmanager.app.entities.SiteEntity;
import com.soprahr.skillmanager.app.entities.UserEntity;
import com.soprahr.skillmanager.app.repositories.SiteRepository;
import com.soprahr.skillmanager.app.repositories.UserRepository;
import com.soprahr.skillmanager.app.services.SiteService;
import com.soprahr.skillmanager.app.shared.dto.SiteDto;
import com.soprahr.skillmanager.app.shared.dto.UserDto;

@Service
public class SiteServiceImpl implements SiteService{

	
	
	@Autowired
	UserRepository userRepositroy;
	
	@Autowired
	SiteRepository siteRepository;
	
	@Override
	public List<SiteDto> getAllSites(String email) {
		UserEntity checkUser = userRepositroy.findByEmail(email);
	    List<SiteDto> SitesListDto = new ArrayList<>();

		if (checkUser == null)
			throw new RuntimeException("User Error Request");
		
		if(checkUser.getRole().getRoleName().equals("Admin")) {
		    List<SiteEntity> sites = (List<SiteEntity>) siteRepository.findAll();
			 for (SiteEntity siteEntity : sites) {
			        ModelMapper modelMapper = new ModelMapper();
			        SiteDto site = modelMapper.map(siteEntity, SiteDto.class);
			        SitesListDto.add(site);
			    }
		}else {
		    SiteEntity siteEntity = siteRepository.findById(checkUser.getSite().getId()).get();
		    ModelMapper modelMapper = new ModelMapper();
	        SiteDto site = modelMapper.map(siteEntity, SiteDto.class);
	        SitesListDto.add(site);
		}
		 return SitesListDto;
	}



	@Override
	public SiteDto getSite(long id) {
		SiteEntity siteEntity = siteRepository.findById(id).get();
		ModelMapper modelMapper = new ModelMapper();
		SiteDto site = modelMapper.map(siteEntity, SiteDto.class);
		return site;
	}
	
	@Override
	public SiteDto createSite(SiteDto siteDto, String email) {
		UserEntity currentUser = userRepositroy.findByEmail(email);
		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = modelMapper.map(currentUser, UserDto.class);
		
		if (userDto.getRole().getRoleName().equals("Collaborateur"))
			throw new RuntimeException("User Role Error !!");
		
		for (SiteEntity site : siteRepository.findAll()) {
			if(site.getName().equals(siteDto.getName())) {
				throw new RuntimeException("Site Already Exist");
			}
		}
		SiteEntity siteEntity = modelMapper.map(siteDto, SiteEntity.class);
		SiteEntity newSite = siteRepository.save(siteEntity);
		SiteDto site = modelMapper.map(newSite, SiteDto.class);
		
		return site;
	}


	@Override
	public SiteDto updateSite(long id, SiteDto siteDto) throws NotFoundException{
		SiteEntity siteEntity = siteRepository.findById(id).get();
		if (siteEntity == null) 			
			throw new NotFoundException();

		siteEntity.setName(siteDto.getName());
		ModelMapper modelMapper = new ModelMapper();
		SiteDto site = modelMapper.map(siteEntity, SiteDto.class);
		return site;
	}
	
	@Override
	public void deleteSite(long id) throws NotFoundException {
		SiteEntity siteEntity = siteRepository.findById(id).get();
		if (siteEntity == null)
			throw new NotFoundException();
		siteRepository.delete(siteEntity);	
	}
}
