package com.soprahr.skillmanager.app.services;
import java.util.List;

import com.soprahr.skillmanager.app.shared.dto.RoleDto;

public interface RoleService {

	List<RoleDto>getAllRoles(String email);
	RoleDto createRole(RoleDto roleDto,String email);
	RoleDto getRole(long id);
	void DeleteRole(long id);
	void DeleteAll(String roleName);
	RoleDto updateRole(long id ,RoleDto roleDto);
}
