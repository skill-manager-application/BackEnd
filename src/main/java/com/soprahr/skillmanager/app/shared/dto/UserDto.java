package com.soprahr.skillmanager.app.shared.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.soprahr.skillmanager.app.entities.DepartmentEntity;
import com.soprahr.skillmanager.app.entities.RoleEntity;
import com.soprahr.skillmanager.app.entities.SiteEntity;

public class UserDto implements Serializable{

	
	private static final long serialVersionUID = 3473374445628358690L;

	private long id;
	private String userId;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String encryptedPassword;
	private RoleEntity role;
	private DepartmentEntity department;
	private SiteEntity site;


	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEncryptedPassword() {
		return encryptedPassword;
	}
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}
	public RoleEntity getRole() {
		return role;
	}
	public void setRole(RoleEntity role) {
		this.role = role;
	}
	public SiteEntity getSite() {
		return site;
	}
	public void setSite(SiteEntity site) {
		this.site = site;
	}
	public DepartmentEntity getDepartment() {
		return department;
	}
	public void setDepartment(DepartmentEntity department) {
		this.department = department;
	}
	 
	
	
	

}
