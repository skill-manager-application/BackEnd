package com.soprahr.skillmanager.app.shared.dto;

import java.util.List;

public class UserDtoListResponse {
    private List<UserDto> userDtoList;
    private int totalPages;
    
	public List<UserDto> getUserDtoList() {
		return userDtoList;
	}
	public void setUserDtoList(List<UserDto> userDtoList) {
		this.userDtoList = userDtoList;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

}
