package com.soprahr.skillmanager.app.shared.dto;

import java.util.List;
import com.soprahr.skillmanager.app.entities.Type;
import com.soprahr.skillmanager.app.responses.UserResponses;

public class DepartmentDto {
	private Long id;
	private String domain;
	  private List<UserResponses> users;
		private Long siteId;
		private Type departmentType;
		private long departmentMotherId;

		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getDomain() {
			return domain;
		}
		public void setDomain(String domain) {
			this.domain = domain;
		}
		public List<UserResponses> getUsers() {
			return users;
		}
		public void setUsers(List<UserResponses> users) {
			this.users = users;
		}
		public Long getSiteId() {
			return siteId;
		}
		public void setSiteId(Long siteId) {
			this.siteId = siteId;
		}
		public Type getDepartmentType() {
			return departmentType;
		}
		public void setDepartmentType(Type departmentType) {
			this.departmentType = departmentType;
		}
		public long getDepartmentMotherId() {
			return departmentMotherId;
		}
		public void setDepartmentMotherId(long departmentMotherId) {
			this.departmentMotherId = departmentMotherId;
		}

}
