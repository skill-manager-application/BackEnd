package com.soprahr.skillmanager.app.requests;

import com.soprahr.skillmanager.app.entities.Type;

public class DepartmentRequest {

	private long siteId;
	private String domain;
	private Type departmentType;
	private long departmentMotherId;

	
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public long getSiteId() {
		return siteId;
	}
	public void setSiteId(long siteId) {
		this.siteId = siteId;
	}
	public Type getDepartmentType() {
		return departmentType;
	}
	public void setDepartmentType(Type departmentType) {
		this.departmentType = departmentType;
	}
	public long getDepartmentMotherId() {
		return departmentMotherId;
	}
	public void setDepartmentMotherId(long departmentMotherId) {
		this.departmentMotherId = departmentMotherId;
	}



}
