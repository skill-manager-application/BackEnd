 package com.soprahr.skillmanager.app.requests;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RoleRequest {
	@NotBlank(message = "Ce Champs Ne doit etre null !")
	@NotEmpty(message = "Ce Champs Ne doit etre null !")
	@NotNull(message = "Ce Champs Ne doit etre null")
	@Size(min = 3,message = "Ce champs doit avoir au moins 3 Caractere !")
	private String roleName;
	@NotBlank(message = "Ce Champs Ne doit etre null !")
	@NotEmpty(message = "Ce Champs Ne doit etre null !")
	@NotNull(message = "Ce Champs Ne doit etre null")
	@Size(min = 3,message = "Ce champs doit avoir au moins 3 Caractere !")
	private String job_title;
	
	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getJob_title() {
		return job_title;
	}

	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}


}
