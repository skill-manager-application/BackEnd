package com.soprahr.skillmanager.app.controllers;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.skillmanager.app.Exceptions.UserExceptions;
import com.soprahr.skillmanager.app.requests.SiteRequest;
import com.soprahr.skillmanager.app.responses.SiteResponse;
import com.soprahr.skillmanager.app.responses.ErrorMessages;
import com.soprahr.skillmanager.app.services.SiteService;
import com.soprahr.skillmanager.app.shared.dto.SiteDto;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/sites")
public class SiteController {

	@Autowired
	SiteService siteService;

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SiteResponse> getSite(@PathVariable Long id) {
		SiteDto SiteDto = siteService.getSite(id);
		SiteResponse SiteResponses = new SiteResponse();
		BeanUtils.copyProperties(SiteDto, SiteResponses);
		return new ResponseEntity<SiteResponse>(SiteResponses, HttpStatus.OK);
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<SiteResponse>> getAllSites(Principal principal) {
		List<SiteResponse> SiteResponses = new ArrayList<>();
		List<SiteDto> SiteDto = siteService.getAllSites(principal.getName());
		Type listType = new TypeToken<List<SiteResponse>>() {
		}.getType();
		SiteResponses = new ModelMapper().map(SiteDto, listType);
		return new ResponseEntity<List<SiteResponse>>(SiteResponses, HttpStatus.OK);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SiteResponse> createSite(@Valid @RequestBody SiteRequest SiteRequest, Principal principal)
			throws Exception {

		if (SiteRequest.getName().isEmpty())
			throw new UserExceptions(ErrorMessages.MISSING_REQUIRED_FILED.getErrorMessage());

		ModelMapper modelMapper = new ModelMapper();
		SiteDto SiteDto = modelMapper.map(SiteRequest, SiteDto.class);
		SiteDto createSite = siteService.createSite(SiteDto, principal.getName());
		SiteResponse userResponse = modelMapper.map(createSite, SiteResponse.class);
		return new ResponseEntity<SiteResponse>(userResponse, HttpStatus.CREATED);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<SiteResponse> updateSite(@PathVariable Long id, @RequestBody SiteRequest SiteRequest)
			throws NotFoundException {
		SiteDto siteDto = new SiteDto();
		BeanUtils.copyProperties(SiteRequest, siteDto);
		SiteDto updateSite = siteService.updateSite(id, siteDto);
		SiteResponse userResponse = new SiteResponse();
		BeanUtils.copyProperties(updateSite, userResponse);
		return new ResponseEntity<SiteResponse>(userResponse, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Object> deleteSite(@PathVariable Long id) throws NotFoundException {
		siteService.deleteSite(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
