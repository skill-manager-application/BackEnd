package com.soprahr.skillmanager.app.controllers;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.soprahr.skillmanager.app.Exceptions.UserExceptions;
import com.soprahr.skillmanager.app.requests.DepartmentRequest;
import com.soprahr.skillmanager.app.responses.DepartmentResponse;
import com.soprahr.skillmanager.app.responses.ErrorMessages;
import com.soprahr.skillmanager.app.services.DepartmentService;
import com.soprahr.skillmanager.app.shared.dto.DepartmentDto;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/departments")
public class DepartmentController {

	@Autowired
	DepartmentService departmentService;

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<DepartmentDto> getDepartment(@PathVariable long id,Principal principal) {
		DepartmentDto departmentDto = departmentService.getDepartment(id,principal.getName());
		return new ResponseEntity<DepartmentDto>(departmentDto, HttpStatus.OK);
	}

	@GetMapping(path = "/site/{siteId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<DepartmentResponse>> getDepartmentBySite(@PathVariable long siteId,
			Principal principal) {
		List<DepartmentResponse> departmentsResponse = new ArrayList<>();
		List<DepartmentDto> departmentDto = departmentService.getDepartmentsBySiteId(siteId, principal.getName());
		Type listType = new TypeToken<List<DepartmentResponse>>() {
		}.getType();
		departmentsResponse = new ModelMapper().map(departmentDto, listType);
		return new ResponseEntity<List<DepartmentResponse>>(departmentsResponse, HttpStatus.OK);
	}

	@GetMapping(path = "/departmentDetails/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<DepartmentResponse> getDepartmentByid(@PathVariable long id,Principal principal) {
		DepartmentDto departmentEntity = departmentService.getDepartment(id,principal.getName());
		DepartmentResponse departmentResponse = new DepartmentResponse();
		BeanUtils.copyProperties(departmentEntity, departmentResponse);

		return new ResponseEntity<DepartmentResponse>(departmentResponse, HttpStatus.OK);
	}

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<DepartmentResponse>> getAllDepartments(Principal principal) {
		List<DepartmentResponse> departmentResponses = new ArrayList<>();
		List<DepartmentDto> departmentDto = departmentService.getAllDepartments(principal.getName());
		Type listType = new TypeToken<List<DepartmentResponse>>() {
		}.getType();
		departmentResponses = new ModelMapper().map(departmentDto, listType);
		return new ResponseEntity<List<DepartmentResponse>>(departmentResponses, HttpStatus.OK);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<DepartmentResponse> createDepartment(@Valid @RequestBody DepartmentRequest departmentRequest,
			Principal principal) throws Exception {

		if (departmentRequest.getDomain().isEmpty())
			throw new UserExceptions(ErrorMessages.MISSING_REQUIRED_FILED.getErrorMessage());
		ModelMapper modelMapper = new ModelMapper();
		DepartmentDto departmentRequestDto = modelMapper.map(departmentRequest, DepartmentDto.class);
		DepartmentDto createDepartment = departmentService.createDepartment(departmentRequestDto, principal.getName());
		DepartmentResponse userResponse = modelMapper.map(createDepartment, DepartmentResponse.class);
		return new ResponseEntity<DepartmentResponse>(userResponse, HttpStatus.CREATED);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<DepartmentResponse> updateDepartment(@PathVariable long id,
			@RequestBody DepartmentRequest departmentRequest) {
		DepartmentDto deparmentDto = new DepartmentDto();
		BeanUtils.copyProperties(departmentRequest, deparmentDto);
		DepartmentDto updateDepartment = departmentService.updateDepartment(id, deparmentDto);
		DepartmentResponse userResponse = new DepartmentResponse();
		BeanUtils.copyProperties(updateDepartment, userResponse);
		return new ResponseEntity<DepartmentResponse>(userResponse, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Object> deleteDepartment(@PathVariable long id) {
		departmentService.DeleteDepartment(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

}
