package com.soprahr.skillmanager.app.controllers;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.soprahr.skillmanager.app.requests.RoleRequest;
import com.soprahr.skillmanager.app.responses.RoleResponse;
import com.soprahr.skillmanager.app.services.RoleService;
import com.soprahr.skillmanager.app.shared.dto.RoleDto;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/roles")
public class RoleController {
	@Autowired
	RoleService roleService;

	@GetMapping
	public ResponseEntity<List<RoleResponse>> getRoles(Principal principal) {
		List<RoleDto> roles = roleService.getAllRoles(principal.getName());
		Type listType = new TypeToken<List<RoleResponse>>() {
		}.getType();
		List<RoleResponse> roleResponse = new ModelMapper().map(roles, listType);
		return new ResponseEntity<List<RoleResponse>>(roleResponse, HttpStatus.OK);
	}

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RoleResponse> getRole(@PathVariable(name = "id") long roleId) {

		RoleDto roleDto = roleService.getRole(roleId);
		ModelMapper modelMapper = new ModelMapper();
		RoleResponse roleResponse = modelMapper.map(roleDto, RoleResponse.class);
		return new ResponseEntity<RoleResponse>(roleResponse, HttpStatus.OK);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE,
			MediaType.MULTIPART_FORM_DATA_VALUE })
	public ResponseEntity<RoleResponse> createRole(@RequestBody RoleRequest roleRequest, Principal principal) {

		ModelMapper modelMapper = new ModelMapper();
		RoleDto roleDto = modelMapper.map(roleRequest, RoleDto.class);
		RoleDto createRole = roleService.createRole(roleDto, principal.getName());
		RoleResponse roleResponse = modelMapper.map(createRole, RoleResponse.class);
		return new ResponseEntity<RoleResponse>(roleResponse, HttpStatus.CREATED);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<RoleResponse> updateRole(@PathVariable(name = "id") long id,
			@RequestBody RoleRequest roleRequest) {
		RoleDto roleDto = new RoleDto();
		BeanUtils.copyProperties(roleRequest, roleDto);
		RoleDto updateRole = roleService.updateRole(id, roleDto);
		RoleResponse roleResponse = new RoleResponse();
		BeanUtils.copyProperties(updateRole, roleResponse);
		return new ResponseEntity<RoleResponse>(roleResponse, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Object> deleteRole(@PathVariable(name = "id") long id) {
		roleService.DeleteRole(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	@DeleteMapping(path="/deleteAll/{name}")
	public ResponseEntity<Object> deleteAllRoles(@PathVariable(name="name") String RoleName){
		roleService.DeleteAll(RoleName);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
