package com.soprahr.skillmanager.app.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.soprahr.skillmanager.app.Exceptions.UserExceptions;
import com.soprahr.skillmanager.app.entities.DepartmentEntity;
import com.soprahr.skillmanager.app.entities.FileEntity;
import com.soprahr.skillmanager.app.entities.RoleEntity;
import com.soprahr.skillmanager.app.entities.SiteEntity;
import com.soprahr.skillmanager.app.entities.UserEntity;
import com.soprahr.skillmanager.app.repositories.DepartmentRepository;
import com.soprahr.skillmanager.app.repositories.RoleRepository;
import com.soprahr.skillmanager.app.repositories.SiteRepository;
import com.soprahr.skillmanager.app.repositories.UserRepository;
import com.soprahr.skillmanager.app.requests.UserRequest;
import com.soprahr.skillmanager.app.responses.ErrorMessages;
import com.soprahr.skillmanager.app.responses.UploadFileResponse;
import com.soprahr.skillmanager.app.responses.UserListResponse;
import com.soprahr.skillmanager.app.responses.UserResponses;
import com.soprahr.skillmanager.app.services.FileService;
import com.soprahr.skillmanager.app.services.UserService;
import com.soprahr.skillmanager.app.shared.dto.UserDto;
import com.soprahr.skillmanager.app.shared.dto.UserDtoListResponse;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	UserService userService;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	DepartmentRepository departmentRepository;

	@Autowired
	SiteRepository siteRepository;
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	private FileService dbFileStorageService;

	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserResponses> getUser(@PathVariable String id) {
		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = userService.getUserByUserId(id);
		UserResponses userResponses = new UserResponses();
		userResponses = modelMapper.map(userDto, UserResponses.class);
		return new ResponseEntity<UserResponses>(userResponses, HttpStatus.OK);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserListResponse> getAllUsers(@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "30") int limit,
			@RequestParam(value = "search", defaultValue = "") String search) {
		List<UserResponses> userResponse = new ArrayList<>();
		UserDtoListResponse users = userService.getUsers(page, limit, search);
		ModelMapper modelMapper = new ModelMapper();
		for (UserDto userDto : users.getUserDtoList()) {
			UserResponses user = modelMapper.map(userDto, UserResponses.class);
			userResponse.add(user);
		}
		UserListResponse listusersWithPages = new UserListResponse();
		listusersWithPages.setUserDtoList(userResponse);
		listusersWithPages.setTotalPages(users.getTotalPages());
		return new ResponseEntity<UserListResponse>(listusersWithPages, HttpStatus.OK);
	}
	@GetMapping(path="/users",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserResponses>> getAllUsersNoLimit(Principal principal) {
		List<UserResponses> userResponse = new ArrayList<>();
		List<UserDto> users = userService.getUsersNoLimit(principal.getName());
		ModelMapper modelMapper = new ModelMapper();
		for (UserDto userDto : users) {
			UserResponses user = modelMapper.map(userDto, UserResponses.class);
			userResponse.add(user);
		}
		return new ResponseEntity<List<UserResponses>>(userResponse, HttpStatus.OK);
	}

	@GetMapping(path = "/department/{departmentId}")
	public ResponseEntity<UserListResponse> getUsersByDepartmentId(@PathVariable long departmentId,
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "10") int limit ,Principal principal) {
		List<UserResponses> userResponse = new ArrayList<>();
		UserDtoListResponse users = userService.getUsersByDeprtmentId(departmentId, page, limit,principal.getName());
		ModelMapper modelMapper = new ModelMapper();
		for (UserDto userDto : users.getUserDtoList()) {
			UserResponses user = modelMapper.map(userDto, UserResponses.class);
			userResponse.add(user);
		}
		UserListResponse listusersWithPages = new UserListResponse();
		listusersWithPages.setUserDtoList(userResponse);
		listusersWithPages.setTotalPages(users.getTotalPages());
		return new ResponseEntity<UserListResponse>(listusersWithPages, HttpStatus.OK);

	}
	
	@GetMapping(path = "/departmentUsers/{departmentId}")
	public ResponseEntity<List<UserResponses>> getUsersByDepartmentIdResponseEntity(@PathVariable long departmentId) {
		List<UserResponses> userResponse = new ArrayList<>();
		List<UserDto> users = userService.getUsersByDeprtmentIdUserResponse(departmentId);
		ModelMapper modelMapper = new ModelMapper();
		for (UserDto userDto : users) {
			UserResponses user = modelMapper.map(userDto, UserResponses.class);
			userResponse.add(user);
		}
		return new ResponseEntity<List<UserResponses>>(userResponse, HttpStatus.OK);

	}
	
	@GetMapping(path="/role/{id}")
	public ResponseEntity<List<UserResponses>> getUsersByRoleId(@PathVariable long id){
		List<UserEntity> users = userRepository.findUsersByRoleIdRole(id);
		List<UserResponses> userResponse = new ArrayList<>();

		ModelMapper modelMapper = new ModelMapper();
		for (UserEntity userDto : users) {
			UserResponses user = modelMapper.map(userDto, UserResponses.class);
			userResponse.add(user);
		}
		return new ResponseEntity<List<UserResponses>>(userResponse, HttpStatus.OK);

		
		

	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserResponses> createUser(@Valid @RequestBody UserRequest userRequest) throws Exception {

		if (userRequest.getFirstname().isEmpty() && userRequest.getEmail().isEmpty()
				&& userRequest.getPassword().isEmpty())
			throw new UserExceptions(ErrorMessages.MISSING_REQUIRED_FILED.getErrorMessage());
		ModelMapper modelMapper = new ModelMapper();
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userRequest, userDto);
		RoleEntity role = roleRepository.findById(userRequest.getRoleId()).get();
		SiteEntity site = siteRepository.findById(userRequest.getSiteId()).get();
		userDto.setRole(role);
		userDto.setSite(site);
		UserDto createUser = userService.createUser(userDto);
		UserResponses userResponse = modelMapper.map(createUser, UserResponses.class);
		return new ResponseEntity<UserResponses>(userResponse, HttpStatus.CREATED);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<UserResponses> updateUser(@PathVariable String id,
			@Valid @RequestBody UserRequest userRequest) {
		UserDto userDto = new UserDto();
		BeanUtils.copyProperties(userRequest, userDto);
		RoleEntity role = roleRepository.findById(userRequest.getRoleId()).get();
		SiteEntity site = siteRepository.findById(userRequest.getSiteId()).get();
		List<DepartmentEntity> department = departmentRepository.findDepartmentBySiteId(site.getId());
		department.forEach((el)->{
			if(el.getDomain().equals("Assining")) {
				userDto.setDepartment(el);
			}
		});
		userDto.setRole(role);
		userDto.setSite(site);
		UserDto updateUser = userService.updateUser(id, userDto);
		UserResponses userResponse = new UserResponses();
		BeanUtils.copyProperties(updateUser, userResponse);
		return new ResponseEntity<UserResponses>(userResponse, HttpStatus.ACCEPTED);
	}

	@DeleteMapping(path = "/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable String id) {
		userService.DeleteUser(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PostMapping("/uploadFile/{userId}")
	public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String userId) {
		FileEntity dbFile = dbFileStorageService.storeFile(file, userId);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(dbFile.getId()).toUriString();

		return new UploadFileResponse(dbFile.getFileName(), fileDownloadUri, file.getContentType(), file.getSize());
	}

	@GetMapping("/downloadFile/{fileId}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {
		// Load file from database
		FileEntity dbFile = dbFileStorageService.getFile(fileId);

		return ResponseEntity.ok().contentType(MediaType.parseMediaType(dbFile.getFileType()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
				.body(new ByteArrayResource(dbFile.getData()));
	}
	
	

}
