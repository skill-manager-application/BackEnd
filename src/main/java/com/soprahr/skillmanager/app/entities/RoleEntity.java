package com.soprahr.skillmanager.app.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name="role")
public class RoleEntity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3114779715068988209L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idRole;
	@Column(nullable = false)
	private String roleName;
	@Column(nullable = false)
	private String job_title;

	
	
	
	public long getIdRole() {
		return idRole;
	}
	public void setIdRole(long idRole) {
		this.idRole = idRole;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getJob_title() {
		return job_title;
	}
	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}
}
