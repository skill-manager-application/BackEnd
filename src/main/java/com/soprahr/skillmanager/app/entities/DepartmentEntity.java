package com.soprahr.skillmanager.app.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="department")
public class DepartmentEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7682628855924763040L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false,length = 50)
	private String domain;
	
	@Column
	private long departmentMotherId;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "site_id")
	private SiteEntity site;

	@Enumerated(EnumType.STRING)
	private Type departmentType;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
		
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
		
	}
	public Type getDepartmentType() {
		return departmentType;
	}

	public void setDepartmentType(Type departmentType) {
		this.departmentType = departmentType;
	}

	public SiteEntity getSite() {
		return site;
	}

	public void setSite(SiteEntity site) {
		this.site = site;
	}


	public long getDepartmentMotherId() {
		return departmentMotherId;
	}


	public void setDepartmentMotherId(long departmentMotherId) {
		this.departmentMotherId = departmentMotherId;
	}

}
